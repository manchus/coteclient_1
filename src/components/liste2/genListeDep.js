import React from 'react';
import { Link } from 'react-router-dom';
import Group from "@material-ui/icons/Group";
class GenListDep extends React.Component {

    render(){
        return (
            <tr>
                <td>{this.props.departement.code}</td>
                <td>{this.props.departement.superviseur}</td>
                <td>{this.props.departement.dep}</td>
                <td>
                <Link to={"/edit2/"+this.props.departement._id}>édition</Link> | 
                 <a href="#" onClick={()=> {this.props.deleteUtil(this.props.departement._id)}}>suppresion</a> | 
                 <Link to={"/utildansdep/"+this.props.departement.dep}><Group/></Link>
                </td>
            </tr>
        )
    }
}
export default GenListDep;