import React from 'react';
import axios from 'axios';
import GenListDep from './genListeDep';

    class ListeDep extends React.Component {
        constructor(props){
            super(props);  
            this.state={
                departements:[]
            }
        this.deleteUtil = this.deleteUtil.bind(this);
        }
    

        componentDidMount(){
            //axios.get('http://10.30.40.121:3437/lireDept')
           axios.get('http://localhost:3437/lireDept')
            .then(response =>{
                //console.log("Departamentos:");
                //console.log(response.data);
                if(response.data.length>0){
                    this.setState({departements:response.data})
                }

            })
            .catch((error)=>{
                console.log(error);
            })
            
        }

        deleteUtil(id){
            //axios.delete('http://10.30.40.121:3437/delDept/'+id)
            axios.delete('http://localhost:3437/delDept/'+id)
            .then(res=>console.log(res.data));
            this.setState({
                departements:this.state.departements.filter(el=>el._id !== id) 
            })
        }

        depList(){
            //console.log("List:");
            //console.log(this.state.departements);
            return this.state.departements.map(utilCourant => {
            return <GenListDep departement={utilCourant} deleteUtil={this.deleteUtil} key={utilCourant.code}/>;
            })
        }


        
    render() {
        return (
            <div className="container">
                <h3>Liste des départements</h3>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>Code</th>
                            <th>Superviseur</th>
                            <th>Département</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.depList()}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ListeDep;