import React from 'react';
import { Link } from 'react-router-dom';
class GenList extends React.Component {

    render(){
        return (
            <tr>
                <td>{this.props.utilisateur.code}</td>
                <td>{this.props.utilisateur.nom}</td>
                <td>{this.props.utilisateur.prenom}</td>
                <td>{this.props.utilisateur.dep}</td>
                <td>
                <Link to={"/edit/"+this.props.utilisateur._id}>édition</Link> |
                 <a href="#" onClick={()=> {this.props.deleteUtil(this.props.utilisateur._id)}}>suppresion</a>
                </td>
            </tr>
        )
    }
}
export default GenList;