import React from 'react';
import axios from 'axios';
import GenList from './genListe';

/*let utils = [   {code:'GreMa',nom:'Grenier',prenom:'Marc'},
                {code:'RoyPa',nom:'Roy',prenom:'Patrick'},
                {code:'HerGe',nom:'Herrera',prenom:'German'},
            ];*/
    class Liste extends React.Component {
        constructor(props){
            super(props);  
            this.state={
                utilisateurs:[]
            }
        this.deleteUtil = this.deleteUtil.bind(this);
        }
    

        componentDidMount(){
            //this.setState({utilisateurts:utils})
            //axios.get('http://10.30.40.121:3437/lireUtil')
            axios.get('http://localhost:3437/lireUtil')
            .then(response =>{
              //  console.log(response.data);
                if(response.data.length>0){
                    this.setState({utilisateurs:response.data})
                }
            })
            .catch((error)=>{
                console.log(error);
            })
        }
        componentWillUnmount(){

        }

        deleteUtil(id){
            //axios.delete('http://10.30.40.121:3437/delUtil/'+id)
            axios.delete('http://localhost:3437/delUtil/'+id)
            .then(res=>console.log(res.data));
            this.setState({
                utilisateurs:this.state.utilisateurs.filter(el=>el._id !== id) 
            })
        }

        userList(){
            return this.state.utilisateurs.map(utilCourant => {
            return <GenList utilisateur={utilCourant} deleteUtil={this.deleteUtil} key={utilCourant.code}/>;
            })
        }


        
    render() {
        return (
            <div className="container">
                <h3>Liste des utilisateurs</h3>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>Code</th>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Département</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.userList()}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Liste;