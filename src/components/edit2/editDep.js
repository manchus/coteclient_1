import React from 'react';
import axios from 'axios';


class EditDep extends React.Component {
    constructor(props) {
        super(props);

        this.onChangeCode = this.onChangeCode.bind(this); //Linking between actionListener and field
        this.onChangeSup = this.onChangeSup.bind(this);
        this.onChangeDep = this.onChangeDep.bind(this);
        this.onSubmit = this.onSubmit.bind(this); 
        this.state = {
            code:'',
            superviseur:'',
            dep:'',
            id:''
        }

    }

    componentDidMount(){
        this.loadDep(); 
}

loadDep = ()=>{
    axios.get('http://10.30.40.121:3437/lireUnDept/' +this.props.match.params.id)
    //axios.get('http://localhost:3437/lireUnDept/' +this.props.match.params.id)
        .then(response =>{
            console.log(response.data);

             this.setState({
                 dep: response.data.dep,
                 code: response.data.code,
                 superviseur: response.data.superviseur,
                 id:this.props.match.params.id
             });
        }).catch((error)=>{
            console.log(error);    
        })
        
}

    onChangeCode(e){
        this.setState({
            code:e.target.value
        })
    }

    onChangeSup(e){
        this.setState({
            superviseur:e.target.value
        })
    }

    onChangeDep(e){
        this.setState({
            dep:e.target.value
        })
    }


    onSubmit(e){
        e.preventDefault();
        const util = {
            code: this.state.code,
            superviseur: this.state.superviseur,
            dep: this.state.dep
        }
        console.log(util);
        axios.post('http://10.30.40.121:3437/upUtil/'+this.props.match.params.id,util)
        //axios.post('http://localhost:3437/upDept/'+this.props.match.params.id,util)
        .then(res=>{
            //console.log(res.data)
            //window.location = "/Liste2";
            window.location.replace('/Liste2');
        });

    }

    render() {
        return (
            <div className="container">
                <h3>Mettre à jour un département</h3>
                <form onSubmit={this.onSubmit}>

                    <div className="form-group">
                        <label>Département:</label>
                        <input type="text"
                        required 
                        className="form-control"
                        value={this.state.dep}
                        onChange={this.onChangeDep}
                        />
                    </div>
                    <div className="form-group">
                        <label>Superviseur:</label>
                        <input type="text"
                        required 
                        className="form-control"
                        value={this.state.superviseur}
                        onChange={this.onChangeSup}
                        />
                    </div>
                    <div className="form-group">
                        <label>Code:</label>
                        <input type="text"
                        required 
                        className="form-control"
                        value={this.state.code}
                        onChange={this.onChangeCode}
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Mettre à jour" className="btn btn-primary"/>
                    </div>
                </form>
            </div>
        )
    }
}
export default EditDep;