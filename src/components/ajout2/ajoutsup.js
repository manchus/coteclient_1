import React from 'react';
import axios from 'axios';


class AjoutSup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            code:'',
            superviseur:'',
            dep:''
        
        }
        
        this.onChangeCode = this.onChangeCode.bind(this); //Linking between actionListener and field
        this.onChangeDep = this.onChangeDep.bind(this);
        this.onChangeSuperviseur = this.onChangeSuperviseur.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }

    onChangeCode(e){
        this.setState({
            code:e.target.value
        })
    }

    onChangeDep(e){
        this.setState({
            dep:e.target.value
        })
    }
    onChangeSuperviseur(e){
        this.setState({
            superviseur:e.target.value
        })
    }
    onSubmit(e){
        e.preventDefault();
        const util = {
            code: this.state.code,
            dep: this.state.dep,
            superviseur: this.state.superviseur
        }
        console.log(util);
        axios.post('http://10.30.40.121:3437/ajoutDept',util)
        //axios.post('http://localhost:3437/ajoutDept',util)
        .then(res=>{
            //console.log(res.data);
            this.setState({
                superviseur:'',
                dep:'',
                code:''
            });
            window.location.replace('/Liste2');
           // window.location = "/Liste2";
        });

    }

    render() {
        return (
            <div className="container">
                <h3>Ajouter un département</h3>
                <form onSubmit={this.onSubmit}>

                    <div className="form-group">
                        <label>Département:</label>
                        <input type="text"
                        required 
                        className="form-control"
                        value={this.state.dep}
                        onChange={this.onChangeDep}
                        />
                    </div>
                    <div className="form-group">
                        <label>Superviseur:</label>
                        <input type="text"
                        required 
                        className="form-control"
                        value={this.state.superviseur}
                        onChange={this.onChangeSuperviseur}
                        />
                    </div>
                    <div className="form-group">
                        <label>Code:</label>
                        <input type="text"
                        required 
                        className="form-control"
                        value={this.state.code}
                        onChange={this.onChangeCode}
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Ajout" className="btn btn-primary"/>
                    </div>
                </form>
            </div>
        )
    }
}
export default AjoutSup;