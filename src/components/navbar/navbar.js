import React from 'react';
import {Link} from 'react-router-dom';
import {NavDropdown} from 'react-bootstrap';

class Navbar extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
                <Link to="/" className="navbar-brand">Bienvenue</Link>
                <div className="navbar-collapse">
                    <ul className="navbar-nav mr-auto">
                        <li className="navbar-item">
                            <Link to="/liste" className="nav-link">Utilisateurs</Link>
                        </li>
                        <li className="navbar-item">
                            <Link to="/ajout" className="nav-link">Ajout d'utilisateurs</Link>
                        </li>
                         <NavDropdown title="Départements" id="basic-nav-dropdown">
                                <NavDropdown.Item href="/liste2">List</NavDropdown.Item>
                                <NavDropdown.Item href="/ajout2">Ajout</NavDropdown.Item>
                                {/* <NavDropdown.Divider /> */}
                        </NavDropdown>

                    </ul>
                </div>
            </nav>
        )
    }
}

export default Navbar;