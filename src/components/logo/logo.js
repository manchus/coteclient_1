import React from 'react';
import reactLogo from './reactlogo.png';

class Logo extends React.Component {
    render() {
        return (
            <div>
                <div className="container">   
                    <div className="container">
                        <a href="https://fr.reactjs.org"><img src={reactLogo} alt="Logo" /></a>
                    </div>
                </div>
            </div>
        )
    }
}

export default Logo;