import React from 'react';
import axios from 'axios';
//import GenListDep from './genListeDep';

    class ListeUtil extends React.Component {
        constructor(props){
            super(props);  
            this.state={
                departements:[],
                utilisateurs:[],
                depto:''
            }
        }
    

        componentDidMount(){
            this.setState({
                depto:this.props.match.params.id
            });

            //axios.get('http://10.30.40.121:3437/lireUtil')
            axios.get('http://localhost:3437/lireUtil')
            .then(response =>{
                if(response.data.length>0){
                    this.setState({utilisateurs:response.data})
                }
                this.setState({utilisateurs: this.state.utilisateurs.filter(util=>util.dep===this.state.depto)}); 
            })
            .catch((error)=>{
                console.log(error);
            })
        }

        

        userList(){
            return this.state.utilisateurs.map(util=>{return <tr key={util._id}  ><td>{util.code}</td><td>{util.nom}</td><td>{util.prenom}</td></tr>} );

        }
      
        render() {
            return (
                <div className="container">
                    <h3>Liste des utilisateurs dans {this.state.depto} ({this.state.utilisateurs.length})</h3>
                    <table className="table">
                        <thead className="thead-light">
                            <tr>
                                <th>Code</th>
                                <th>Nom</th>
                                <th>Prénom</th>
                            </tr>
                        </thead>
                        <tbody>
                        {this.userList()}
                        </tbody>
                    </table>
                </div>
            )
        }
}

export default ListeUtil;