import React from 'react';
import { Link } from 'react-router-dom';
class GenList2 extends React.Component {

    render(){
        return (
            <tr>
                <td>{this.props.departement.code}</td>
                <td>{this.props.departement.superviseur}</td>
                <td>{this.props.departement.dep}</td>
            </tr>
        )
    }
}
export default GenList2;