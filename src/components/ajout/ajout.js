import React from 'react';
import axios from 'axios';


class Ajout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            code:'',
            nom:'',
            prenom:'',
            dep:'',
            departements:[],
            ldep:[]
        
        };

        this.onChangeCode = this.onChangeCode.bind(this); //Linking between actionListener and field
        this.onChangeNom = this.onChangeNom.bind(this);
        this.onChangePrenom = this.onChangePrenom.bind(this);
        this.onChangeDep = this.onChangeDep.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount(){
       
         this.loadDep(); 
    }

    loadDep = ()=>{
        axios.get('http://10.30.40.121:3437/lireDept')
        //axios.get('http://localhost:3437/lireDept')
        .then(response =>{
            //console.log(response.data);
            if(response.data.length>0){
                this.setState({departements:response.data})          
            }
            var newArr = this.state.departements.map(dep2=>dep2.dep).filter(dep2=>dep2!==undefined);
            this.setState({ldep: Array.from(new Set(newArr)).sort()});
        })
        .catch((error)=>{
            console.log(error);
        })
    }

    onChangeCode(e){
        this.setState({
            code:e.target.value
        })
    }

    onChangeNom(e){
        this.setState({
            nom:e.target.value
        })
    }
    onChangePrenom(e){
        this.setState({
            prenom:e.target.value
        })
    }
    onChangeDep(e){
        this.setState({
            dep:e.target.value
        })
    }
    onSubmit(e){
        e.preventDefault();

        const util = {
            code: this.state.code,
            nom: this.state.nom,
            prenom: this.state.prenom,
            dep: this.state.dep?this.state.dep:this.state.ldep[0]
        }
        console.log(util);
        axios.post('http://10.30.40.121:3437/ajoutUtil',util)
        //axios.post('http://localhost:3437/ajoutUtil',util)
        .then(res=>{console.log(res.data)
            this.setState({
                dep:'',
                prenom:'',
                nom:'',
                code:''
            });
            window.location.replace('/liste');
        });
    }

    displayDep= (deptlist) =>{
             return deptlist.map((departement,index) => <option key={index} value={departement} >{departement}</option>)
         }  
    
    render() {
        
        return (
            <div className="container">
                <h3>Ajouter un utilisateur</h3>

                <form onSubmit={this.onSubmit}>

                    <div className="form-group">
                        <label>Départements:</label>
                        <select className="form-control" options={this.state.dep}  
                        value={this.state.dep?this.state.dep:"Any"} onChange={this.onChangeDep}>
                        {this.displayDep(this.state.ldep)}
                        </select>
                    </div>
                    <div className="form-group">
                        <label>Nom:</label>
                        <input type="text"
                        required 
                        className="form-control"
                        value={this.state.nom}
                        onChange={this.onChangeNom}
                        />
                    </div>
                    <div className="form-group">
                        <label>Prénom:</label>
                        <input type="text"
                        required 
                        className="form-control"
                        value={this.state.prenom}
                        onChange={this.onChangePrenom}
                        />
                    </div>
                    <div className="form-group">
                        <label>Code:</label>
                        <input type="text"
                        required 
                        className="form-control"
                        value={this.state.code}
                        onChange={this.onChangeCode}
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Ajout" className="btn btn-primary"/>
                    </div>
                </form>
            </div>
        )
    }
}
export default Ajout;