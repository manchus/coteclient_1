import React from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css"
import Liste from './components/liste/liste';
import ListeDep from './components/liste2/listeDep';
import ListeUtil from './components/utildansdep/listeutil';
import Ajout from './components/ajout/ajout';
import AjoutSup from './components/ajout2/ajoutsup';
import Edit from './components/edit/edit';
import EditDep from './components/edit2/editDep';
import Navbar from './components/navbar/navbar';
import Logo from './components/logo/logo';  //ListeUtil
import {Redirect} from "react-router-dom"



function App() {
  return (
    <div className="container">
    <Router>
    <Route exact path="/">
                    <Redirect to="/logo" /> : <Liste/>
                </Route>
      <Navbar/>
      <Logo/>
      <Route exact path="/liste" component={Liste} />
      <Route exact path="/Ajout" component={Ajout} />
      <Route exact path="/liste2" component={ListeDep} />
      <Route exact path="/utildansdep/:id" component={ListeUtil} />
      <Route exact path="/Ajout2" component={AjoutSup} />
      <Route exact path="/Edit/:id" component={Edit} />
      <Route exact path="/Edit2/:id" component={EditDep} />
    </Router>
  </div>
  );
}

export default App;
